ages = ['15', '16', '17', '18']
print(f" My age is it {ages[0]}")


# 修改列表的元素
ages[0] = '19'
print(ages)

# 从末尾添加列表元素
ages.append('20')
print(ages)

# 从从列表任意位置添加元素
ages.insert(5,'21')
print(ages)

# 从列表中删除元素
del ages[5]
print(ages)

# 使用pop方法删除元素
Motorcycles = ['Honda', 'Kawasaki', 'BMW', 'Ducati']
Take_Ducati = Motorcycles.pop()
print(Motorcycles)
print(Take_Ducati)

# 使用pop方法弹出任意位置的元素
Take_BMW = Motorcycles.pop(2)
print(Motorcycles)
print(Take_BMW)

# 根据值删除元素
Motorcycles.remove('Honda')
print(Motorcycles)

# 列表永久正位排序
letters = ['B', 'A', 'C', 'E', 'G', 'R', 'F']
letters.sort()
print(letters)

# 使用sortd 对列表进行临时排序
letters = ['B', 'A', 'C', 'E', 'G', 'R', 'F']
print(sorted(letters))
print(letters)

# 倒着打印列表
letters.reverse()
print(letters)

# 确定列表长度
print(len(letters))

# 遍历整个列表
automobiles = ['BMW', 'Ferrari', 'Mercedes Benz', 'Audi', 'Lamborghini', 'senna', 'Bugatti']
for automobile in automobiles:
    print(automobile)

# 在for循环中执行更多的操作
for automobile in automobiles:
    print(f"I like {automobile}")

# 使用range函数
for value in range(1,5):
    print(value)

# 使用range创建数字列表
numbers = list(range(1,11))
print(numbers)

squares = []
for value in range(1,11):
    square = value ** 2
    squares.append(square)
print(squares)

# 对数字列表执行简单的统计计算
print(min(numbers))
print(max(numbers))
print(sum(numbers))

# 列表解析
squares = [value **2 for value in range(1,11)]

# 切片
Fruits = ['Banana', 'apple', 'Sydney', 'grape', 'guava', 'Pitaya']
print(Fruits[0:3])

# 遍历切片
for Fruit in Fruits[0:]:
    print(Fruit)

# 复制列表
copy_lists = Fruits[:]
print(copy_lists)

# 元组
rectangles = (200,50)
print(rectangles[0])
print(rectangles[1])

# 遍历
for rectangle in rectangles:
    print(rectangle)

# 修改元组变量
# 虽然不能修改元组的元素，但是可以给存储元素重新赋值。因此，如果要修改前述矩形的尺寸，可重新定义整个元组：
rectangles = (200,50)
print(rectangles)

rectangles = (400,100)
print(rectangles)

# 简单的if语句
age = 19
if age>=18:
    print("You are 18 years old")

# if else 语句
age = 17
if age >= 18:
    print("You are old enough to votel !")
    print("have you registered to vote yet?")
else:
    print("Sorry, you are too yourg to vote.")
    print("Please register to vote as soon as you turn 18!")

# if elif else 结构
age = 12

if age < 4:
    print("Your admission cost is $0")
elif age < 18:
    print("Your admission cost is $25")
else:
    print("Your admission cost is $40")

# 使用if语句处理列表
# 检查特殊元素
requested_toppings = ['mushrooms', 'green peppers', 'extra cheese']

for requested_topping in requested_toppings:
    print(f"Adding {requested_topping}.")

print("\n Finished making your pizza !")

requested_toppings = ['mushrooms', 'green peppers', 'extra cheese']
for requested_topping in requested_toppings:
    if requested_topping == 'green peppers':
        print("Sorry ,we are out of green peppers right now.")
    else:
        print(f"Adding {requested_topping}")

print("\nFinished making your pizza")

# 确定列表不是空的
requested_toppings = []

if requested_toppings:
    for requested_topping in requested_toppings:
        print(f"Adding {requested_topping}")
    print("\nFinished making your pizza")
else:
    print("Are you sure you want a plain pizza!")

# 使用多个列表
available_toppings = ['mushrooms', 'olives', 'green peppers',
                      'pepperoni', 'pineapple', 'extra cheese']

requested_toppings = ['mushrooms', 'green peppers', 'extra cheese']

for requested_topping in requested_toppings:
    if requested_topping in available_toppings:
        print(f"Adding {requested_topping}")
    else:
        print(f"Soryy ,we don't have {requested_topping}")

print("\nFinished making your pizza!")

# 一个简单的字典
alien_0 = {'color':'green', 'points':5}

print(alien_0['color'])
print(alien_0['points'])

# 添加键值对
alien_0 = {'color':'green', 'points':5}

alien_0['x_position'] = 0
alien_0['y_position'] = 25
print(alien_0)

# 修改字典中的值
alien_0['color'] = 'yellow'
print(f"The alien is now {alien_0['color']}")

# 删除键值对
del alien_0['color']
print(alien_0)

# 使用类似对象组成的字典
favorite_languages = {
    'jen':'python',
    'sarah':'c',
    'edward':'ruby',
    'phil':'python',
}
print(favorite_languages['sarah'])

# 使用get() 来访问值
alien_0 = {'color':'green', 'speed':'slow'}
point_value = alien_0.get('poins','No point value assigned.')  # 在不确定键值对是否存在字典中的时候使用
print(point_value)

# 遍历字典
user_0 = {
    'username':'efermi',
    'firs':'enrico',
    'last':'fermi'
}

for key,value in user_0.items():
    print(f"\nKey:{key}")
    print(f"\nValue:{value}")

# 遍历字典中所以的键
user_0 = {
    'username':'efermi',
    'firs':'enrico',
    'last':'fermi'
}
for key in user_0.keys():
    print(f"\n{key}")


